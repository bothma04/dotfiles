
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'andrewradev/splitjoin.vim'
Plugin 'bling/vim-bufferline'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'easymotion/vim-easymotion'
Plugin 'edkolev/promptline.vim'
Plugin 'ervandew/supertab'
Plugin 'godlygeek/tabular'
Plugin 'jiangmiao/auto-pairs'
Plugin 'joshdick/onedark.vim'
Plugin 'nicwest/qq.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'plasticboy/vim-markdown'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'sheerun/vim-polyglot'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-speeddating'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-unimpaired'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" " The following are examples of different formats supported.
" " Keep Plugin commands between vundle#begin/end.
" " plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" " plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" " Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" " git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" " The sparkup vim script is in a subdirectory of this repo called vim.
" " Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" " Install L9 and avoid a Naming conflict if you've already installed a
" " different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}
"
" " All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
" "filetype plugin on
" "
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=2
" when indenting with '>', use 4 spaces width
set shiftwidth=2
" On pressing tab, insert 4 spaces
set expandtab

" Folding
set foldmethod=syntax
set number rnu
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline_theme='dark'
set encoding=utf-8
colorscheme onedark
set ttimeoutlen=10
let g:promptline_preset = {
      \'a'    : [ '\h'  ],
      \'b'    : [ '\u'  ],
      \'c'    : [ '\w'  ]}

" Fuzzy find
set path+=**
set wildmenu

" Wrap Guide
set colorcolumn=81,82

" Tags
command! MakeTags !ctags -R --exclude=node_modules .
vnoremap ; :
nnoremap ; :
" no <up> ddkP
" no <down> ddp
no <left> <Nop>
no <right> <Nop>

ino <up> <Nop>
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>

vno <up> <Nop>
vno <down> <Nop>
vno <left> <Nop>
vno <right> <Nop>

" Make these auto-center
" nmap G Gzz
nmap n nzz
nmap N Nzz
nmap { {zz
nmap } }zz

" Functino derp
iabbr functino function
iabbr functoin function
set cursorline
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_section_z = '%3p%% %3l/%L:%3v'
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
set showcmd

let g:airline_mode_map = {
    \ '__' : '-',
    \ 'n'  : 'N',
    \ 'i'  : 'INSERT',
    \ 'R'  : 'REPLACE',
    \ 'c'  : 'C',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ }


:map <S-F4> :%s/<C-r><C-w>//gc<Left><Left><Left>

syntax on

highlight LineNr ctermfg=196 ctermbg=16
highlight Comment ctermfg=226
highlight Normal ctermfg=255 ctermbg=16

let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exec = 'eslint_d'
let g:erlang_highlight_special_atoms = 1

let g:bufferline_solo_highlight = 1
let g:bufferline_pathshorten = 1
let g:bufferline_rotate = 2

" Context aware pointer style for iTerm
let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"

" VIM KOANS

" Master Wq and the Windows developer
"
"
" Master Wq was addressing some Vim novices. After his lecture on the many
" virtues of Vim, he asked if there were any questions. A young man raised his
" hand.
"
" "Master, by what means might one filter for the second column of a plaintext"
" "table for all rows that contain the string 'tcp'?"
"
" Master Wq said nothing, turned to the whiteboard behind him, and wrote:
"
" :%!awk '/tcp/ && NF>=2 {print $2}'
"
" There was a murmur of approval from the other students.
"
" "But I develop on Windows..." the student stammered.
"
" Master Wq turned again, erased the command, and wrote:
"
" :v/tcp/d
" :v/^\s*\S\+\s\+\(\S\+\).*/d
" :%s//\1/
"
" "What! That is far too complex for such a simple task!" cried the student.
"
" Master Wq turned again, erased the command, and wrote:
"
" Microsoft Excel
"
" At once, the student was enlightened.



" Master Wq and the Markdown acolyte
"
"
" A Markdown acolyte came to Master Wq to demonstrate his Vim plugin.
"
" "See, master," he said, "I have nearly finished the Vim macros that translate"
" "Markdown into HTML. My functions interweave, my parser is a paragon of"
" "efficiency, and the results nearly flawless. I daresay I have mastered"
" "Vimscript, and my work will validate Vim as a modern editor for the"
" "enlightened developer! Have I done rightly?"
"
" Master Wq read the acolyte’s code for several minutes without saying anything.
" Then he opened a Markdown document, and typed:
"
" :%!markdown
"
" HTML filled the buffer instantly. The acolyte began to cry.
