#!/bin/bash

# Basic tree-view of directory, works good in workspace (soft-linked) folders and git repos
# alias twig='tree -C --dirsfirst --sort="name" -I "node_modules" -L 3'
alias nano="nano --softwrap --nohelp --linenumbers --autoindent --tabsize=2 --smooth --nowrap --boldtext --quickblank --positionlog"
alias ..="cd .."

if [[ $OSTYPE == 'darwin16' ]]; then
	alias la="ls -Gla"
	alias ll="ls -lG"
	alias plantuml="java -jar /Users/stephan/io/consent/libraries/plantuml/target/plantuml-8057-SNAPSHOT.jar"
fi
alias vbox="VBoxManage"

# create a directory and immediately move into it
mcd () {
  mkdir -p $1 && cd $1
}

twig () {
  clear
	if [ -f readme.md ]; then
		echo ""
		head -n 20 readme.md
	fi
	tree -C --dirsfirst --sort="name" -I ".git|node_modules" -L 3 "$@"
}

jurls () {
  clear
  echo "$1"
  curl -s "$1" | jq
}

schema () {
  clear
  echo "schema.cnsnt.io/$1"
  curl -s "schema.cnsnt.io/$1" | jq
}

alias curls="curl -s"
# alias jurls="curl -s $1 | jq"

