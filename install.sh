#!/bin/bash

# VIM
ln -Fi .vimrc        ~/.vimrc

# Bash runs .bashrc whenever it is started interactively, contrast to
# .bash_profile & .profile which only run at start of new login shells
ln -Fi .bashrc       ~/.bashrc
ln -Fi .bash_aliases ~/.bash_aliases


# tmux
ln -Fi .tmux.conf         ~/.tmux.conf
ln -Fi .tmux.conf.local   ~/.tmux.conf.local

# Experimental shell prompt
ln -Fi .shell_prompt.sh   ~/.shell_prompt.sh

# Gitconfig
ln -Fi .gitconfig ~/.gitconfig
